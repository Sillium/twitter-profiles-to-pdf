 #!/bin/bash
# new line instead of comma
tr ',' '\n' < $1 > $2.tmp.1

# trim whitespaces
cat $2.tmp.1 | ./trim.sh > $2.tmp.2

# sort ignoring case
sort -f -u $2.tmp.2 > $2.tmp.3

mv $2.tmp.3 $2

rm $2.tmp.*