# Usage

## Create single image

`casperjs --name=Sillium --zoom=2 --target=images/avatars/Sillium.png`

## Prepare list of users

`./prepareList.sh list_original.txt list.txt`

## Create A4 PDFs

`./fetchList.sh list.txt`