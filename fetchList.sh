#!/bin/bash

IMAGES_PER_COLUMN=2
COLUMNS_PER_PAGE=5

AVATAR_DIR='avatars'
STITCH_DIR='stitch'
FINAL_DIR='final'

mkdir -p images/{${AVATAR_DIR},${STITCH_DIR},${FINAL_DIR}}

counter=1
pageCounter=1
rowCounter=1
columnCounter=1

rm list_not_found.txt

function log {
    echo "$1 - $2"
}

while read name; do
    log $name "fetching HTTP status code"
    httpStatus=$(curl -s -o /dev/null -w "%{http_code}" https://twitter.com/$name)
    log $name "HTTP status code: $httpStatus"

    if [ "${httpStatus}" -ne 200 ]; then
        echo $name >> list_not_found.txt
        continue
    fi

    counterFormatted=$(printf "%03d" ${counter})
    pageCounterFormatted=$(printf "%02d" ${pageCounter})
    columnCounterFormatted=$(printf "%02d" ${columnCounter})
    rowCounterFormatted=$(printf "%02d" ${rowCounter})
    targetFile=images/${AVATAR_DIR}/${counterFormatted}-P${pageCounterFormatted}_C${columnCounterFormatted}_R${rowCounterFormatted}-${name}

    log $name "calling CasperJS"
    casperjs casper-bio.js --name=$name --zoom=2 --target=${targetFile}

    width=$(identify -format "%w" ${targetFile}_bio.png)> /dev/null
    height=$(identify -format "%w" ${targetFile}_bio.png)> /dev/null
    bgColor=$(identify -format "%[pixel: u.p{0,0}]" ${targetFile}_bio.png)

    log $name "resizing ${targetFile}_avatar.png to bio's width $width"
    convert ${targetFile}_avatar.png -gravity center -resize $width ${targetFile}_avatar.png

    log $name "adding border to top of ${targetFile}_bio.png"
    convert ${targetFile}_bio.png -gravity south -background ${bgColor} -extent $(identify -format '%Wx%[fx:H+10]' ${targetFile}_bio.png) ${targetFile}_bio.png

    log $name "adding border to bottom of ${targetFile}_bio.png"
    convert ${targetFile}_bio.png -gravity north -background ${bgColor} -extent $(identify -format '%Wx%[fx:H+20]' ${targetFile}_bio.png) ${targetFile}_bio.png

    log $name "combining bio and avatar to ${targetFile}.png"
    montage ${targetFile}_avatar.png ${targetFile}_bio.png -tile 1x2 -geometry +0+0 -gravity center ${targetFile}.png

    log $name "deleting bio and avatar pngs"
    rm ${targetFile}_avatar.png ${targetFile}_bio.png

    log $name "adding white 15px border"
    mogrify -mattecolor white -frame 15x15 -format png ${targetFile}.png

    log $name "adding grey 1px border"
    mogrify -bordercolor grey -border 1 -format png ${targetFile}.png

    counter=$[counter + 1]
    rowCounter=$[rowCounter + 1]
    if (( ${rowCounter} > ${IMAGES_PER_COLUMN} )); then
        columnCounter=$[columnCounter + 1]
        rowCounter=1
    fi

    if (( $columnCounter > ${COLUMNS_PER_PAGE} )); then
        pageCounter=$[pageCounter + 1]
        columnCounter=1
        rowCounter=1
    fi
done < $1

echo "pageCounter:$pageCounter"

for (( page = 1; page <= $pageCounter; page++ ))
do  
    pageFormatted=$(printf "%02d" $page)
    for (( column = 1; column <= $COLUMNS_PER_PAGE; column++ ))
    do  
        columnFormatted=$(printf "%02d" $column)
        echo "Creating column $column of page $page."
        montage images/${AVATAR_DIR}/*-P${pageFormatted}_C${columnFormatted}_R*.png -tile 1x${IMAGES_PER_COLUMN} -geometry +0+0 -gravity NorthWest images/${STITCH_DIR}/P${pageFormatted}_C${columnFormatted}.png
    done
    echo "Creating page $page from columns."
    # Combining columns of page
    montage images/${STITCH_DIR}/P${pageFormatted}_C*.png -tile ${COLUMNS_PER_PAGE}x1 -geometry +0+0 -gravity NorthWest images/${STITCH_DIR}/P${pageFormatted}.png
    # Resizing page to width of DIN-A4 @ 300dpi in landscape orientation
    convert images/${STITCH_DIR}/P${pageFormatted}.png -gravity NorthWest -background white -resize 3508 images/${STITCH_DIR}/page_${pageFormatted}_A4.png
    # Adding white space to page to get height of DIN-A4 @ 300dpi in landscape orientation
    convert images/${STITCH_DIR}/page_${pageFormatted}_A4.png -gravity NorthWest -background white -extent x2480 images/${FINAL_DIR}/page_${pageFormatted}_A4.png
done

convert images/${FINAL_DIR}/page_*_A4.png images/complete.pdf

rm images/${AVATAR_DIR}/*.png
rm images/${STITCH_DIR}/*.png
rm images/${FINAL_DIR}/*.*



