var casper = require('casper').create({
    verbose: true,
    logLevel: 'debug'
});

var args = {
    name: casper.cli.get('name'),
    zoom: casper.cli.get('zoom'),
    target: casper.cli.get('target')
};

casper.start().zoom(args.zoom).thenOpen('https://twitter.com/' + args.name).then(function() {
    var boundsHeaderCard = this.getElementBounds('div.ProfileHeaderCard');
    var boundsName = this.getElementBounds('h1.ProfileHeaderCard-name');
    var boundsScreenname = this.getElementBounds('h2.ProfileHeaderCard-screenname');
    var boundsBio = this.getElementBounds('p.ProfileHeaderCard-bio');
    var boundsLocation = this.getElementBounds('div.ProfileHeaderCard-location');

    var constants = {
        paddingAvatar: 25,
        paddingBio: 11
    };

    var paddingSum = constants.paddingBio;
    if (boundsLocation.height > 0) {
        paddingSum += constants.paddingBio;
    }
    if (boundsBio.height > 0) {
        paddingSum += constants.paddingBio;
    }

    var boundsBioWithPadding = {
        top: boundsHeaderCard.top - constants.paddingBio,
        width: boundsHeaderCard.width,
        left: boundsHeaderCard.left,
        height: boundsName.height + boundsScreenname.height + boundsBio.height + boundsLocation.height + (paddingSum * args.zoom)
    };
    
    casper.capture(args.target + '_bio.png', {top: boundsBioWithPadding.top, left: boundsBioWithPadding.left, width: boundsBioWithPadding.width, height: boundsBioWithPadding.height}, {format: 'png', quality: 100});

    var boundsAvatar = this.getElementBounds('div.ProfileCanopy-avatar');

    var boundsAvatarWithPadding = {
        top: boundsAvatar.top - constants.paddingAvatar,
        width: boundsAvatar.width + (constants.paddingAvatar * 2),
        left: boundsAvatar.left - constants.paddingAvatar,
        height: boundsAvatar.height + constants.paddingAvatar
    };

    casper.capture(args.target + '_avatar.png', {top: boundsAvatarWithPadding.top, left: boundsAvatarWithPadding.left, width: boundsAvatarWithPadding.width, height: boundsAvatarWithPadding.height}, {format: 'png', quality: 100});
});

casper.run();
